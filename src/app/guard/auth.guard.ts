import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { StorageService } from '../services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor( private storageService: StorageService, private router: Router){

  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):  boolean  {
      if(this.storageService.obtenerUsuario()){
        return true
      } else {
        Swal.fire('Usuario no autenticado', `Asegurese de estar logueado para ingresar a ${route.url}`,'error')
        this.router.navigateByUrl('/login');
      }
  }
  
}
