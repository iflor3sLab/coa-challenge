import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
//Componentes
import { ProductosComponent } from '../components/productos/productos.component';
import { LoginComponent } from '../components/login/login.component';
import { RegistrarComponent } from '../components/registrar/registrar.component';
import { AuthGuard } from '../guard/auth.guard';
import { CartComponent } from '../components/cart/cart.component';
import { OrdersComponent } from '../components/orders/orders.component';

const routes: Routes = [
    { path: 'login', component: LoginComponent},
    { path: 'productos', component: ProductosComponent, canActivate: [ AuthGuard ]},
    { path: 'cart', component: CartComponent, canActivate: [ AuthGuard ]},
    { path: 'orders', component: OrdersComponent, canActivate: [ AuthGuard ]},
    { path: 'registrar', component: RegistrarComponent},
    { path: '**', pathMatch: 'full', redirectTo: 'productos'}
  ];

  @NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })

  export class AppRoutingModule { }