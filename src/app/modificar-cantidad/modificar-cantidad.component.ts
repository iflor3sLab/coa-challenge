import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { CartService } from '../services/cart.service';
import { ContadorComponent } from '../shared/contador/contador.component';

@Component({
  selector: 'app-modificar-cantidad',
  templateUrl: './modificar-cantidad.component.html',
  styleUrls: ['./modificar-cantidad.component.css'],
})
export class ModificarCantidadComponent implements OnInit {
  
  @ViewChild(ContadorComponent) contadorChild: ContadorComponent;

  public dialogRef: MatDialogRef<ModificarCantidadComponent>;
  constructor(
    private matDialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private cartService: CartService
  ) {}

  ngOnInit(): void {

  }
  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    this.contadorChild.forma.get('cantidad').valueChanges.subscribe(data => {
      this.data.cantidad = data
    })
    
  }

  onNoClick(): void {
    this.matDialog.closeAll();
  }

}
