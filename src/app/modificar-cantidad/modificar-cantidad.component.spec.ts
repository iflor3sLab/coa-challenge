import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificarCantidadComponent } from './modificar-cantidad.component';

describe('ModificarCantidadComponent', () => {
  let component: ModificarCantidadComponent;
  let fixture: ComponentFixture<ModificarCantidadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModificarCantidadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificarCantidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
