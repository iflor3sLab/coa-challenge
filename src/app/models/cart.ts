export enum Status{
    PENDING = 'pending',
    COMPLETED = 'completed'
}

export class Cart{
    public id: string
    public status: Status
}