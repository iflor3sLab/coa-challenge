export class Order{
    public cart_id: string
    public product_id: number
    public quantity: number
}