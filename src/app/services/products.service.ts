import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Producto } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private afs: AngularFirestore) {
   

  }

  obtenerProductos(): Observable<Producto[]>{
    return this.afs.collection<Producto>('products').valueChanges()
  }

  obtenerProductosxId(ids: number[]): Observable<Producto[]>{
    return this.afs.collection<Producto>('products', ref => ref.where('id', 'in', ids)).valueChanges()
    
  }



  

}
