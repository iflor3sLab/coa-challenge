import { Injectable } from '@angular/core';
import { User } from 'firebase';

const KEYUSERSTORAGE = 'user'

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  guardarUsuario(usuarioData: User ){
    sessionStorage.setItem(KEYUSERSTORAGE, JSON.stringify(usuarioData))
  }

  obtenerUsuario(){
    if (sessionStorage.getItem(KEYUSERSTORAGE)){

      const usuario = sessionStorage.getItem(KEYUSERSTORAGE ) 
      return JSON.parse(usuario) as User
    } else return null
  }

  borrarUsuario(){
    sessionStorage.getItem(KEYUSERSTORAGE) ? 
      sessionStorage.removeItem(KEYUSERSTORAGE ) : null
  }

    
}
