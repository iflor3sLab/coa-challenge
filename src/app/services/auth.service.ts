import { Injectable } from '@angular/core';
import { auth } from 'firebase/app';
import { User } from 'firebase';
import { AngularFireAuth } from '@angular/fire/auth';
import { first } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { StorageService } from './storage.service';
import Swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
@Injectable({
  providedIn: 'root',
})
export class AuthService {

  private usuario: BehaviorSubject<User> = new BehaviorSubject<User>(null);

  constructor(private firebaseAuth: AngularFireAuth, private storageService: StorageService,
    private toast: ToastrService) {}

  async login(email: string, password: string) {
    try {
      const resultado = await this.firebaseAuth.signInWithEmailAndPassword(
        email,
        password
      );
      this.toast.success('Bienvenido','Login exitoso',{
        progressBar: true,

      })
      await this.getCurrentUser()
      return resultado;
    } catch (error) {
      Swal.fire('Error en login',error.message,'error')
    }
  }

  async logout() {
    try {
      this.storageService.borrarUsuario()
      await this.firebaseAuth.signOut();
      this.toast.info('Hasta luego','Logout exitoso',{
        progressBar: true,
      })
      this.getCurrentUser()
    } catch (error) {
      Swal.fire('Error en logout',error.message,'error')
    }
  }

  async register(email: string, password: string) {
    
    try {
      const result = await this.firebaseAuth.createUserWithEmailAndPassword(
        email,
        password
      );
      await this.getCurrentUser()
      return result;
    } catch (error) {
      Swal.fire('Error en registro',error.message,'error')
    }
  }



  async getCurrentUser() {
    try {   
      const usuario =  await this.firebaseAuth.authState.pipe(first()).toPromise();
  
      this.updateDataUser(usuario)
      return usuario;
    } catch (error) {
      console.log(error);
    }
  }

  updateDataUser(data: User): void {
    this.usuario.next(data);
    
  }

  getData(): Observable<User> {
    return this.usuario;
  }

  

}
