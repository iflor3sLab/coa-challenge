import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ProductoInCart } from '../models/productInCart';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/firestore';
import { Order } from '../models/order';
import { Cart, Status } from '../models/cart';
import Swal from 'sweetalert2';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class CartService {
  productos: ProductoInCart[] = [];

  constructor(private toast: ToastrService, private afs: AngularFirestore) {}

  addToCart(obj: ProductoInCart) {
    if (
      this.productos.find((element) => element.producto.id == obj.producto.id)
    ) {
      this.productos.find(
        (element) => element.producto.id == obj.producto.id
      ).cantidad += obj.cantidad;
      this.toast.success(
        `Agregaste ${obj.cantidad} a un articulo en el carrito`,
        'Agregaste'
      );
    } else {
      this.productos.push(obj);
      this.toast.success(
        `Agregaste ${obj.cantidad} ${obj.producto.name} al carrito`,
        'Agregaste'
      );
    }
  }

  modificar(id: number, cantidad: number) {
    const prodIndex = this.productos.findIndex(
      (element) => element.producto.id == id
    );
    if (prodIndex >= 0) {
      this.productos[prodIndex].cantidad = cantidad;
    }
  }

  removerProducto(id: number) {
    const prodIndex = this.productos.findIndex(
      (element) => element.producto.id == id
    );
    if (prodIndex >= 0) {
      this.productos.splice(prodIndex, 1);
      console.log('despues de remover', this.productos);
    }
  }

  async subirOrdenFirebase() {
    if (this.productos.length == 0) return;
    console.log('intento subir orden');
    try {
      Swal.fire({
        allowOutsideClick: false,
        title: 'Subiendo la orden a la db',
        text: 'Aguarde por favor',
        icon: 'info',
      });
      Swal.showLoading();
      const CARTSCOLLECTION = this.afs.collection<Cart>('carts');
      const CARTSPRODSCOLLECTION = this.afs.collection<Order>('product_carts');

      const cart: Cart = {
        id: 'ID-TEMPORAL',
        status: Status.PENDING,
      };
      const docCartRef = await CARTSCOLLECTION.add(cart);
      await docCartRef.update({ id: docCartRef.id });

      this.productos.forEach(async (prod) => {
        const order: Order = {
          cart_id: docCartRef.id,
          product_id: prod.producto.id,
          quantity: prod.cantidad,
        };
        await CARTSPRODSCOLLECTION.add(order);
      });

      Swal.fire(
        'Orden cargada correctamente',
        'Guarde el siguiente codigo para buscar su orden: ' + docCartRef.id,
        'success'
      );
    } catch (err) {
      console.log(err);
      Swal.fire('Error en crear orden', err.message, 'error');
    }
  }

  getDataOrden(codigo: string) {
    if (!codigo) return;

    return this.afs
      .collection<Order>('product_carts', (ref) => ref.where('cart_id', '==', codigo))
      .valueChanges()
  }

  getCartxId(id: string){
    return this.afs.collection<Cart>('carts',ref => ref.where('id', '==', id)).valueChanges()
    .pipe( map(res => res[0]))
  }

  removerAll(){
    this.productos = []
  }
}
