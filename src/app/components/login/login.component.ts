import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

 

  forma = new FormGroup({})
  model = {
    email: '',
    password: '',
  }
  fields: FormlyFieldConfig[] = [
    {
      key: 'email',
      type: 'input',
      templateOptions: {
        type: 'text',
        required: true,
        label: 'Correo'
      }
    },
    {
      key: 'password',
      type: 'input',
      templateOptions:{
        type:'password',
        label: 'Contrasena',
        minLength: 6,
        required: true
      }
    },
    

  ]

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit({valid, value}){
    if (!valid) return ;
    const {email, password} = value
    this.authService.login(email, password)
      .then(res => {
        if(res && res.user){
          this.router.navigateByUrl('/productos')
        }
      })
      
  }


  
  
  

}
