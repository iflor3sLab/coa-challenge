import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { FormlyFieldConfig } from '@ngx-formly/core';

//SERVICIOS
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css']
})
export class RegistrarComponent implements OnInit {

  forma = new FormGroup({})

  model = {
    email: '',
    password: '',
    repassword: ''
  }

  fields: FormlyFieldConfig[] = [
    {
      key: 'email',
      type: 'input',
      templateOptions: {
        type: 'text',
        label: 'Correo'
      }
    },
    {
      key: 'password',
      type: 'input',
      templateOptions:{
        type:'password',
        label: 'Contrasena'
      }
    },
    {
      key: 'repassword',
      type: 'input',
      templateOptions:{
        type:'password',
        label: 'Repetir Contrasena'
      }
    }

  ]

  

  constructor( private authService: AuthService, private router: Router ) { }

  ngOnInit(): void {
  }

  onSubmit({valid, value}){
    const {email, password} = value
    this.authService.register(email, password)
    .then( res => {
      if (res.user) {
        this.router.navigateByUrl('productos')
      }
    })
  }
}
