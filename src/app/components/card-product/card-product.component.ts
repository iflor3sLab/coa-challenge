import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Producto } from 'src/app/models/product';
import { ContadorComponent } from '../../shared/contador/contador.component';
import { ProductoInCart } from '../../models/productInCart';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-card-product',
  templateUrl: './card-product.component.html',
  styleUrls: ['./card-product.component.css']
})
export class CardProductComponent implements OnInit {

  constructor(private cartService: CartService) { }
  
  @Input() producto: Producto
  @ViewChild(ContadorComponent) contadorHijo : ContadorComponent

  ngOnInit(): void {
  }

  addToCart(){
    const prodInCart: ProductoInCart = {
      cantidad :this.contadorHijo.model.cantidad,
      producto: this.producto
    }
    this.cartService.addToCart(prodInCart)
    this.contadorHijo.forma.reset()
  }
}
