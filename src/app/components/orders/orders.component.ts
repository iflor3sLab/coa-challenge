import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { CartService } from '../../services/cart.service';
import { ProductsService } from '../../services/products.service';
import { Producto } from '../../models/product';
import { map } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { Cart } from 'src/app/models/cart';
export interface ItemOrder {
  nombre: string;
  cantidad: number;
}
@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css'],
})
export class OrdersComponent implements OnInit {
  forma = new FormGroup({});
  model = {
    codigoOrden: '',
  };
  fields: FormlyFieldConfig[] = [
    {
      key: 'codigoOrden',
      type: 'input',
      templateOptions: {
        type: 'text',
        required: true,
        label: 'Codigo de orden',
      },
    },
  ];

  dataSource: ItemOrder[] = [];
  cartData: Cart
  displayedColumns: string[] = ['nombre', 'cantidad'];
  columnsToDisplay: string[] = this.displayedColumns.slice();
  constructor(
    private cartService: CartService,
    private productService: ProductsService,
    private toast: ToastrService
  ) {}

  ngOnInit(): void {}

  onSubmit({ valid, value }) {
    if (!valid) return;

    this.cargarTabla(value.codigoOrden);
  }

  cargarTabla(codigo: string) {
    Swal.fire({
      title: 'Trayendo datos orden',
      text: 'Aguarde un momento',
      allowOutsideClick: false,
      icon: 'info',
    });

    Swal.showLoading();

    const items: ItemOrder[] = [];
    this.cartService.getDataOrden(codigo).subscribe((data) => {
      if (data.length == 0) {
        Swal.close()
        this.toast.error('No hay datos de la orden ' + codigo ,'No se encontro la orden');
        return;
      }
      this.cartService.getCartxId(codigo).subscribe( cartRes => {
        this.cartData = cartRes
      })

      this.productService
        .obtenerProductosxId(data.map((e) => e.product_id))
        .subscribe((prods) => {
          prods.forEach((p) => {
            items.push({
              cantidad: data.find((element) => element.product_id == p.id)
                .quantity,
              nombre: p.name,
            });
          });
          this.dataSource = items;
          Swal.close();
        });
    });
  }
}
