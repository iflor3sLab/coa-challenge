import { Component, OnInit } from '@angular/core';
import { Status } from 'src/app/models/cart';
import { ProductsService } from '../../services/products.service';
import { Producto } from 'src/app/models/product';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {

  productos: Producto[] = []
  
  constructor(private productService: ProductsService) { 
  }

  ngOnInit(): void {
    this.getProductos()
  }

  getProductos(){
    Swal.fire({
      title: 'Aguarde',
      text: 'Estamos cargando los productos',
      icon: 'info',
      allowOutsideClick: false
    })
    Swal.showLoading()

    this.productService.obtenerProductos().subscribe(data => {
      this.productos = data
      Swal.close()
    }, 
    err => {
      console.error(err)
      Swal.fire('Error en productos', err.message ? err.message : 'Ocurrio un error', 'error' )
    })
  }

}
