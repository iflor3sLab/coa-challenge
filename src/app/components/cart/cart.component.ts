import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, Inject, OnInit } from '@angular/core';
import { ColDef } from 'ag-grid-community';
import { CartService } from '../../services/cart.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Producto } from 'src/app/models/product';
import { ModificarCantidadComponent } from 'src/app/modificar-cantidad/modificar-cantidad.component';
import { ProductoInCart } from '../../models/productInCart';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

export interface PeriodicElement {
  nombre: string;
  precio: number;
  cantidad: number;
  fecha: string;
  descripcion: string;
}

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})

export class CartComponent implements OnInit {
  cartDataSource: any[]
  showTable: boolean = true
  columnsToDisplay = ['name', 'cantidad', 'price', 'accion'];

  constructor(public cartService: CartService, public dialog: MatDialog, private router: Router) {}

  ngOnInit(): void {
    this.cartDataSource= this.cartService.productos
    // this.cartService.subirOrdenFirebase()
  }

  borrarProducto(prod: Producto){
    Swal.fire({
      title: 'Estas seguro de quitar el producto?',
      text: "No podras revertir esta operacion!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.cartService.removerProducto(prod.id)
        // this.cartDataSource= this.cartService.productos
      this.refrescar()

        
        Swal.fire(
          'Borrado!',
          'Se quito el articulo correctamente',
          'success'
        )
      }
    })
  }

  editarProducto(prod: Producto, cant: number){
    const dialogRef = this.dialog.open(ModificarCantidadComponent, {
      width: '250px',
      data: {name: prod.name, cantidad: cant }
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed', result);
      if (result){

        this.cartService.modificar(prod.id, result.cantidad)
      }
      // this.cartDataSource= this.cartService.productos
    });
  }

  refrescar(){
    this.showTable = false,
    setTimeout(() => {
      this.showTable = true
    }, 1000);
  }

  crearOrden(){
    this.cartService.subirOrdenFirebase().then( res => {
      this.cartService.removerAll()
      this.router.navigateByUrl('/orders')
    })

  }
}


