import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { User } from 'firebase';
import { StorageService } from 'src/app/services/storage.service';
import { Router } from '@angular/router';
import { CartService } from '../../services/cart.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  usuario: User;
  constructor(
    private authService: AuthService,
    private storageService: StorageService,
    private router: Router,
    public cartService: CartService
  ) {}

  ngOnInit(): void {
    this.authService.getData().subscribe((resp) => {
      if (resp) {
        this.usuario = resp;
        this.storageService.guardarUsuario(resp);
      } else {
        this.usuario = this.storageService.obtenerUsuario();
      }
    });
  }

  logout() {
    this.authService.logout();
    this.router.navigateByUrl('/login');
  }
}
