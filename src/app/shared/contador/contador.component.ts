import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';

@Component({
  selector: 'app-contador',
  templateUrl: './contador.component.html',
  styleUrls: ['./contador.component.css']
})
export class ContadorComponent implements OnInit {

  @Input() contadorInicio : number

  forma = new FormGroup({})
  model = {
    cantidad: null
  }
  fields: FormlyFieldConfig[] = [
    {
      key: 'cantidad',
      type: 'input',
      templateOptions: {
        type: 'number',
        label: 'Cantidad',
        min: 0
      }
    }
  ]

  constructor() {
  
   }

  ngOnInit(): void {
    if(this.contadorInicio){
      this.model.cantidad = this.contadorInicio
    }
  }

}
