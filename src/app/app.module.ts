import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormlyFieldConfig, FormlyModule } from '@ngx-formly/core';
import { FormlyMaterialModule } from '@ngx-formly/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './modules/material/material.module';
import { LoginComponent } from './components/login/login.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { ProductosComponent } from './components/productos/productos.component';
import { AppRoutingModule } from './router/app-routing.module';
import { RegistrarComponent } from './components/registrar/registrar.component';

import { AngularFireModule } from '@angular/fire';
import {AngularFireAuthModule} from '@angular/fire/auth'
import { environment } from '../environments/environment';
import { ToastrModule } from 'ngx-toastr';
import { CardProductComponent } from './components/card-product/card-product.component';
import { ContadorComponent } from './shared/contador/contador.component';
import { CartComponent } from './components/cart/cart.component';
import { OrdersComponent } from './components/orders/orders.component';
import { AgGridModule } from 'ag-grid-angular';
import { ModificarCantidadComponent } from './modificar-cantidad/modificar-cantidad.component';

export function minLengthValidationMessage(err, field: FormlyFieldConfig){
  return `Debe tener al menos ${err.minLength}`
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    ProductosComponent,
    RegistrarComponent,
    CardProductComponent,
    ContadorComponent,
    CartComponent,
    OrdersComponent,
    ModificarCantidadComponent
  ],
  imports: [
    BrowserModule,
    AgGridModule,
    ReactiveFormsModule,
    FormlyModule.forRoot({
      validationMessages:[
        {
          name: 'required',
          message: 'Este campo es obligatorio'
        },
        {
          name: 'minLength',
          message: minLengthValidationMessage
        }
      ]
    }),
    FormlyMaterialModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right',
      timeOut: 5000
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
